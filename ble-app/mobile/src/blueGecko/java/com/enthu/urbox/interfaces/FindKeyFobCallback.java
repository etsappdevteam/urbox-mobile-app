package com.enthu.urbox.interfaces;

import com.enthu.urbox.ble.BluetoothDeviceInfo;

public interface FindKeyFobCallback {
    void findKeyFob(BluetoothDeviceInfo fob);

    void triggerDisconnect();

    String getDeviceName();
}
