package com.enthu.urbox;

import android.os.Bundle;

public interface Location {
    void onLocationChanged(android.location.Location location);

    void onProviderDisabled(String provider);

    void onProviderEnabled(String provider);

    void onStatusChanged(String provider, int status, Bundle extras);

}
