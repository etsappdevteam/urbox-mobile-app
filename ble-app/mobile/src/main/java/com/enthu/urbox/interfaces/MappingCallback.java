package com.enthu.urbox.interfaces;

import com.enthu.urbox.mappings.Mapping;

public interface MappingCallback {
    void onNameChanged(Mapping mapping);
}
