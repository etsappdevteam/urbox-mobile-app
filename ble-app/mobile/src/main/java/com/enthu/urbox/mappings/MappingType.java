package com.enthu.urbox.mappings;

public enum MappingType {
    SERVICE,
    CHARACTERISTIC
}
