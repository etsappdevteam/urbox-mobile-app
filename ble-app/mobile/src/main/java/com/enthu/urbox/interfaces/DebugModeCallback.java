package com.enthu.urbox.interfaces;

import com.enthu.urbox.ble.BluetoothDeviceInfo;

public interface DebugModeCallback {

    void connectToDevice(BluetoothDeviceInfo device);

    void addToFavorite(String deviceAddress);

    void removeFromFavorite(String deviceAddress);

    void addToTemporaryFavorites(String deviceAddress);

    void updateCountOfConnectedDevices();
}
