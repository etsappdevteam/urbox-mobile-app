package com.enthu.urbox;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.enthu.urbox.activity.BrowserActivity;
import com.enthu.urbox.bluetoothdatamodel.parsing.Device;
import com.enthu.urbox.bluetoothdatamodel.parsing.Engine;
import com.enthu.urbox.services.Write;

import java.util.Arrays;

import static com.enthu.urbox.services.BluetoothLeService.ACTION_GATT_CONNECTED;
import static com.enthu.urbox.services.BluetoothLeService.ACTION_GATT_CONNECTION_STATE_ERROR;
import static com.enthu.urbox.services.BluetoothLeService.ACTION_GATT_DISCONNECTED;


public class IndoorVer3 extends AppCompatActivity implements LocationListener {
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    EditText text, batLowtext, textHigh, textv3,net,app,indkey;
    Button btnv3, btnnet,btnapp,btnindkey,buttonlatvr3,bttnlongv3,btnconf;
    BluetoothGatt magattService;

    TextView helpButton;
    Button Latbtn, longbtn;
    TextView textlatver3, txtlongv3,lattxt,longtxt;
    String lat;
    String provider;
    protected String latitude, longitude;
    protected boolean gps_enabled, network_enabled;
    private Dialog helpDialog;
    private Dialog hiddenDebugDialog;
    private String DEVICE_ADDRESS;
    private Dialog alertDialogView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indoor_ver3);

        textv3 = findViewById(R.id.Device);
        net = findViewById(R.id.net);
        app = findViewById(R.id.app);
        indkey = findViewById(R.id.indoorkey);
        textlatver3= findViewById(R.id.Lativ3);
        txtlongv3 = findViewById(R.id.Lonv3);

        btnv3 = findViewById(R.id.deviceUpdate);
        btnnet = findViewById(R.id.netkeyUpdate);
        btnapp = findViewById(R.id.appkeyUpdate);
        btnindkey = findViewById(R.id.indoorkeyUpdate);
        buttonlatvr3= findViewById(R.id.LatUpdver3);
        bttnlongv3 = findViewById(R.id.LongUpdv3);



        magattService = Write.gattService;

        if (magattService == null) {

            Toast.makeText(this, "Device Has benn disconnected ", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, BrowserActivity.class);
            startActivity(i);
        }



        //========================== Device Address  ========================

        btnv3.setOnClickListener(new View.OnClickListener() {
            @Override
        public void onClick(View v) {
            String value = textv3.getText().toString();
                if (value.length() == 8 && !(value.length()==0)) {


                        byte[] newValue = value.getBytes();
                        System.out.println(newValue);

                        System.out.println(Arrays.toString(newValue));
                        Write.SSID.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                        Write.SSID.setValue(newValue);
                        textv3.setText("");
                        //    writeCharacteristic(Write.batLowThershold);
                        if (value != null) {

                            byte[] newValue1 = value.getBytes();

                            try {
                                Write.SSID.setValue(newValue1);
                                magattService.writeCharacteristic(Write.SSID);

                            } catch (Exception e) {
                                Log.e("Service", "null" + e);
                            }
                        } else {
                            Write.SSID.setValue(value);
                            magattService.writeCharacteristic(Write.SSID);
                        }
                        Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                        Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();


                }else {
                    textv3.setError("Enter valid Address");
                }
            }
        });
        //=====================================================



        //========================== Net Key  ========================

        btnnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = net.getText().toString();
                if (value.length() == 28 && !(value.length()==0)) {


                    byte[] newValue = value.getBytes();
                    System.out.println(newValue);

                    System.out.println(Arrays.toString(newValue));
                    Write.Password.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                    Write.Password.setValue(newValue);
                    net.setText("");
                    //    writeCharacteristic(Write.batLowThershold);
                    if (value != null) {

                        byte[] newValue1 = value.getBytes();

                        try {
                            Write.Password.setValue(newValue1);
                            magattService.writeCharacteristic(Write.Password);

                        } catch (Exception e) {
                            Log.e("Service", "null" + e);
                        }
                    } else {
                        Write.Password.setValue(value);
                        magattService.writeCharacteristic(Write.Password);
                    }
                    Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();


                }else {
                    net.setError("Enter valid Key");
                }
            }
        });
        //=====================================================


        //========================== App Key  ========================

        // here im using the writeconf  as indoor uuid for app key //    -->  1c00

        btnapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = app.getText().toString();
                if (value.length() == 28 && !(value.length()==0)) {


                    byte[] newValue = value.getBytes();
                    System.out.println(newValue);

                    System.out.println(Arrays.toString(newValue));
                    Write.Indoorid.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                    Write.Indoorid.setValue(newValue);
                    app.setText("");
                    //    writeCharacteristic(Write.batLowThershold);
                    if (value != null) {

                        byte[] newValue1 = value.getBytes();

                        try {
                            Write.Indoorid.setValue(newValue1);
                            magattService.writeCharacteristic(Write.Indoorid);

                        } catch (Exception e) {
                            Log.e("Service", "null" + e);
                        }
                    } else {
                        Write.Indoorid.setValue(value);
                        magattService.writeCharacteristic(Write.Indoorid);
                    }
                    Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();


                }else {
                    app.setError("Enter valid Key");
                }
            }
        });
        //=====================================================

        //========================== Indoor id  ========================

        // here im using the write conf  as outdoor uuid for inddorid key //    -->  1d00

        btnindkey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = indkey.getText().toString();

                System.out.println(value.length());
                if (value.length() == 5 && !(value.length() == 0)) {


                    byte[] newValue = value.getBytes();
                    System.out.println(newValue);

                    System.out.println(Arrays.toString(newValue));
                    Write.Outdoorid.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                    Write.Outdoorid.setValue(newValue);
                    indkey.setText("");
                    //    writeCharacteristic(Write.batLowThershold);
                    if (value != null) {

                        byte[] newValue1 = value.getBytes();

                        try {
                            Write.Outdoorid.setValue(newValue1);
                            magattService.writeCharacteristic(Write.Outdoorid);

                        } catch (Exception e) {
                            Log.e("Service", "null" + e);
                        }
                    } else {
                        Write.Outdoorid.setValue(value);
                        magattService.writeCharacteristic(Write.Outdoorid);
                    }
                    Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();


                }else {
                    indkey.setError("Enter valid ID");
                }


            }
        });
        //=====================================================

        /////////////// Location /////////////

        buttonlatvr3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = textlatver3.getText().toString();
                System.out.println(value.length());
                if (value.length() <=50 && !(value.length() == 0)) {


                    byte[] newValue = value.getBytes();
                    System.out.println(newValue);

                    System.out.println(Arrays.toString(newValue));
                    Write.locLatThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                    Write.locLatThershold.setValue(newValue);
                    textlatver3.setText("");
                    //    writeCharacteristic(Write.batLowThershold);
                    if (value != null) {

                        byte[] newValue1 = value.getBytes();

                        try {
                            Write.locLatThershold.setValue(newValue1);
                            magattService.writeCharacteristic(Write.locLatThershold);

                        } catch (Exception e) {
                            Log.e("Service", "null" + e);
                        }
                    } else {
                        Write.locLatThershold.setValue(value);
                        magattService.writeCharacteristic(Write.locLatThershold);
                    }
                    Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                }


            }
        });

//////////////// Long ///////////////////////////////

        bttnlongv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = txtlongv3.getText().toString();
                System.out.println(value.length());
                if (value.length() <=50 && !(value.length() == 0)) {

                    byte[] newValue = value.getBytes();
                    System.out.println(newValue);

                    System.out.println(Arrays.toString(newValue));
                    Write.locLongThershold.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
                    Write.locLongThershold.setValue(newValue);
                    txtlongv3.setText("");
                    //    writeCharacteristic(Write.batLowThershold);
                    if (value != null) {

                        byte[] newValue1 = value.getBytes();

                        try {
                            Write.locLongThershold.setValue(newValue1);
                            magattService.writeCharacteristic(Write.locLongThershold);

                        } catch (Exception e) {
                            Log.e("Service", "null" + e);
                        }
                    } else {
                        Write.locLongThershold.setValue(value);
                        magattService.writeCharacteristic(Write.locLongThershold);
                    }
                    Toast.makeText(getApplicationContext(), "Please Wait.......", Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Updated Successfully", Toast.LENGTH_LONG).show();
                }


            }
        });



        //=====================================================
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(IndoorVer3.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(IndoorVer3.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, IndoorVer3.this);


    }


    @Override
    public void onLocationChanged(Location location) {
        textlatver3 = (TextView) findViewById(R.id.Lativ3);
        txtlongv3 = (TextView) findViewById(R.id.Lonv3);

        textlatver3.setText("" + location.getLatitude());
        txtlongv3.setText("" + location.getLongitude());
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude","status");
    }
    public boolean writeCharacteristic(BluetoothGattCharacteristic charac){

        //check mBluetoothGatt is available
        if (magattService == null) {
            Log.e("", "lost connection");
            return false;
        }
//        if (Service == null) {
//            Log.e("", "service not found!");
//            return false;
//        }
        if (charac == null) {
            Log.e("", "char not found!");
            return false;
        }

        boolean status = magattService.writeCharacteristic(charac);
        return status;
    }

    public byte[] hexToByteArray(String hex) {

        if (hex.length() != 0 && hex.length() % 2 != 0) {
            hex = "0" + hex;
        }

        int len =hex.length();

        byte[] byteArr = new byte[len];
        for (int i = 0; i < byteArr.length; i++) {
            int init = i * 2;
            int end = init + 2;
            int temp = Integer.parseInt(hex.substring(init, end), 16);
            byteArr[i] = (byte) (temp & 0xFF);
        }
        return byteArr;
    }
    // Converts string given in decimal system to byte array
    private byte[] decToByteArray(String dec) {
        if (dec.length() == 0) {
            return new byte[]{};
        }

        String data[] = new String[4];
        for (int i = 0;i < dec.length(); i++){
            data[i]= String.valueOf(dec.charAt(i));
        }
        byte[] byteArr = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            try {
                byteArr[i] = (byte) (Integer.parseInt(data[i]));
            } catch (NumberFormatException e) {
                return new byte[]{0};
            }
        }
        return byteArr;
    }
    private void showCustomDialog() {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.activity_help, viewGroup, false);



        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        private Device connectedDevice;

        // Called when device has changed connection status and appropriate
        // broadcast with device address extra is sent
        // It can be either connected or disconnected state
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    Device device = Engine.getInstance().getDevice(gatt);
                    device.setConnected(true);
                    connectedDevice = device;
                    Intent updateIntent = new Intent(ACTION_GATT_CONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    sendBroadcast(updateIntent);
                    gatt.discoverServices();
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Device device = Engine.getInstance().getDevice(gatt);
                    device.setConnected(false);
                    Intent updateIntent = new Intent(ACTION_GATT_DISCONNECTED);
                    updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                    if (device.equals(connectedDevice)) {
                        connectedDevice = null;
                    }
                    sendBroadcast(updateIntent);
                }
            } else {
                Device device = Engine.getInstance().getDevice(gatt);
                Intent updateIntent = new Intent(ACTION_GATT_CONNECTION_STATE_ERROR);
                updateIntent.putExtra(DEVICE_ADDRESS, device.getAddress());
                sendBroadcast(updateIntent);
            }
            Log.i("BLE service", "onConnectionStateChange - status: " + status + " - new state: " + newState);

        }};



}